# Colocalization tracker

Analytical application for tracking moving points 
in 2D area and finding their co-existence (colocalization). 

## Build
Application is written in Go language. 
To build and run executable application you 
have to [download and install](https://golang.org/doc/install)
Go first. To create executable binary run build command inside `cmd` directory:

	Windows:    env GOOS=windows GOARCH=amd64 go build -o app_win.exe main.go
	MacOS:      env GOOS=darwin GOARCH=amd64 go build -o app_mac main.go

## Run
Run executable inside `cmd` directory with command `./app_win` or `./app_mac` 
based on your operation system from command line or terminal. Application can be run with parameters:

    d	distance               - max points distance
	b	blinking               - max colocalization frame gap
	f	filename               - input CSV data filename
	sx	shiftX                 - X axis shift
	sy	shiftY                 - Y axis shift
	p	passivityRadius        - passivity radius
	roix	regionOfInterestX      - region of interest X
	roiy	regionOfInterestY      - region of interest Y
	roir	regionOfInterestRadius - region of interest radius

Example:

    Windows:  ./app_win -f=data.csv -d=12 -b=0
    MacOS:    ./app_mac -f=data.csv -d=12 -b=0

## Input data
Input CSV file must be placed inside `data/input` directory.
CSV column delimiter is set to `;` and columns must be as follows 
`point_id; frame; X_axis; Y_axis; color; movie_id;` where color is `GRN` or `RED`.
