package config

import (
	"errors"
	"flag"
	"regexp"
)

var (
	EmptyFilenameErr                 = errors.New("empty filename")
	InvalidFilenameErr               = errors.New("invalid filename")
	ZeroRegionOfInterestRadiusErr    = errors.New("region of interest radius 'roir' must be greater than 0 if parameter 'roix' or 'roiy' is set")
	InvalidRegionOfInterestRadiusErr = errors.New("region of interest radius 'roir' must be greater than 0")
)

type Config struct {
	Distance               float64
	Blinking               uint64
	Filename               string
	ShiftX                 float64
	ShiftY                 float64
	PassivityRadius        float64
	RegionOfInterestX      float64
	RegionOfInterestY      float64
	RegionOfInterestRadius float64
}

func Init() (*Config, error) {
	var (
		distance               = flag.Float64("d", 0.1, "max points distance")
		blinking               = flag.Uint64("b", 1, "blinking - max colocalization frame gap")
		filename               = flag.String("f", "", "input CSV data filename")
		shiftX                 = flag.Float64("sx", 0.0, "X axis shift")
		shiftY                 = flag.Float64("sy", 0.0, "Y axis shift")
		passivityRadius        = flag.Float64("p", 0.0, "passivity radius")
		regionOfInterestX      = flag.Float64("roix", -1.0, "region of interest X")
		regionOfInterestY      = flag.Float64("roiy", -1.0, "region of interest Y")
		regionOfInterestRadius = flag.Float64("roir", 0.0, "region of interest radius")
	)

	flag.Parse()

	if *blinking < 1 {
		*blinking = 1
	}

	if err := validateRegionOfInterest(*regionOfInterestX, *regionOfInterestY, *regionOfInterestRadius); err != nil {
		return nil, err
	}

	if *regionOfInterestX < 0 {
		*regionOfInterestX = 0
	}

	if *regionOfInterestY < 0 {
		*regionOfInterestY = 0
	}

	if err := validateFilename(*filename); err != nil {
		return nil, err
	}

	config := &Config{
		Filename:               *filename,
		Distance:               *distance,
		Blinking:               *blinking,
		ShiftX:                 *shiftX,
		ShiftY:                 *shiftY,
		PassivityRadius:        *passivityRadius,
		RegionOfInterestX:      *regionOfInterestX,
		RegionOfInterestY:      *regionOfInterestY,
		RegionOfInterestRadius: *regionOfInterestRadius,
	}

	return config, nil
}

func validateFilename(filename string) error {
	if filename == "" {
		return EmptyFilenameErr
	}

	match, err := regexp.MatchString("^.*\\.(csv|CSV)$", filename)
	if err != nil {
		return err
	}

	if !match {
		return InvalidFilenameErr
	}

	return nil
}

func validateRegionOfInterest(regionOfInterestX, regionOfInterestY, regionOfInterestRadius float64) error {
	if regionOfInterestRadius < 0 {
		return InvalidRegionOfInterestRadiusErr
	}

	if (regionOfInterestX >= 0 || regionOfInterestY >= 0) && regionOfInterestRadius == 0 {
		return ZeroRegionOfInterestRadiusErr
	}

	return nil
}
