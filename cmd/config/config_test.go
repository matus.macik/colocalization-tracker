package config

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func Test_validateFilename(t *testing.T) {
	tcs := []struct {
		name     string
		filename string
		expErr   error
	}{
		{
			name:     "ok",
			filename: "file.csv",
			expErr:   nil,
		},
		{
			name:     "empty",
			filename: "",
			expErr:   EmptyFilenameErr,
		},
		{
			name:     "invalid",
			filename: "file",
			expErr:   InvalidFilenameErr,
		},
	}

	for _, tc := range tcs {
		t.Run(tc.name, func(t *testing.T) {
			err := validateFilename(tc.filename)

			if tc.expErr != nil {
				assert.EqualError(t, err, tc.expErr.Error())
			} else {
				assert.Nil(t, err)
			}
		})
	}
}

func Test_validateRegionOfInterest(t *testing.T) {
	tcs := []struct {
		name   string
		x      float64
		y      float64
		r      float64
		expErr error
	}{
		{
			name:   "ok",
			x:      1,
			y:      1,
			r:      1,
			expErr: nil,
		},
		{
			name:   "invalid region of interest radius",
			x:      1,
			y:      1,
			r:      -1,
			expErr: InvalidRegionOfInterestRadiusErr,
		},
		{
			name:   "zero region of interest radius",
			x:      0,
			y:      1,
			r:      0,
			expErr: ZeroRegionOfInterestRadiusErr,
		},
	}

	for _, tc := range tcs {
		t.Run(tc.name, func(t *testing.T) {
			err := validateRegionOfInterest(tc.x, tc.y, tc.r)

			if tc.expErr != nil {
				assert.EqualError(t, err, tc.expErr.Error())
			} else {
				assert.Nil(t, err)
			}
		})
	}
}
