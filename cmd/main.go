package main

import (
	"encoding/csv"
	"fmt"
	"io"
	"log"
	"os"
	"sort"
	"strconv"
	"strings"

	"app/cmd/config"
	"app/internal/colocalization"
	"app/internal/color"
	"app/internal/point"
	"app/internal/trajectory"
)

func main() {
	cfg, err := config.Init()
	if err != nil {
		log.Fatal(err)
	}

	printConfig(cfg)

	cfg.Blinking += 1

	t, err := loadCSV(fmt.Sprintf("../data/input/%s", cfg.Filename), cfg.ShiftX, cfg.ShiftY)
	if err != nil {
		log.Fatal(err)
	}

	var counters = make(map[int]int)

	// --------------------------------------------------
	// Colocalized all trajectories
	// --------------------------------------------------

	if err := writeCSV(fmt.Sprintf("../data/output/%s_colocalized_all_trajectories.csv", cfg.Filename), []string{"Id", "Frame", "X", "Y", "Intensity", "Color", "Movie"}, func() [][]string {
		var ret [][]string

		for _, ot := range t.Trajectories() {
			if !ot.IsRegionOfInterest(cfg.RegionOfInterestX, cfg.RegionOfInterestY, cfg.RegionOfInterestRadius) {
				continue
			}

			for _, ct := range t.Trajectories() {
				if !ct.IsRegionOfInterest(cfg.RegionOfInterestX, cfg.RegionOfInterestY, cfg.RegionOfInterestRadius) {
					continue
				}

				for _, c := range colocalization.Colocalizations(ot, ct, cfg.Distance, cfg.Blinking) {
					counters[c.Len()] += 1
					for i := 0; i < len(c.ObservedTrajectory); i++ {
						ret = append(ret, c.ObservedTrajectory[i].StringSlice())
						ret = append(ret, c.ColocalizedTrajectory[i].StringSlice())
					}
				}
			}
		}

		return ret
	}(), ';'); err != nil {
		log.Fatal(err)
	}

	// --------------------------------------------------
	// Colocalized all trajectories lengths
	// --------------------------------------------------

	if err := writeCSV(fmt.Sprintf("../data/output/%s_colocalized_all_trajectories_lengths.csv", cfg.Filename), []string{"Trajectory length", "Count"}, func() [][]string {
		var ret [][]string

		keys := make([]int, 0, len(counters))
		for k := range counters {
			keys = append(keys, k)
		}
		sort.Ints(keys)

		for _, k := range keys {
			ret = append(ret, []string{strconv.Itoa(k), strconv.Itoa(counters[k])})
		}

		return ret
	}(), ';'); err != nil {
		log.Fatal(err)
	}

	// --------------------------------------------------
	// Colocalized passive trajectories
	// --------------------------------------------------

	counters = make(map[int]int)

	if err := writeCSV(fmt.Sprintf("../data/output/%s_colocalized_passive_trajectories.csv", cfg.Filename), []string{"Id", "Frame", "X", "Y", "Intensity", "Color", "Movie"}, func() [][]string {
		var ret [][]string

		for _, ot := range t.Trajectories() {
			if !ot.IsRegionOfInterest(cfg.RegionOfInterestX, cfg.RegionOfInterestY, cfg.RegionOfInterestRadius) || !ot.IsPassive(cfg.PassivityRadius) {
				continue
			}

			for _, ct := range t.Trajectories() {
				if !ct.IsRegionOfInterest(cfg.RegionOfInterestX, cfg.RegionOfInterestY, cfg.RegionOfInterestRadius) || !ct.IsPassive(cfg.PassivityRadius) {
					continue
				}

				for _, c := range colocalization.Colocalizations(ot, ct, cfg.Distance, cfg.Blinking) {
					counters[c.Len()] += 1
					for i := 0; i < len(c.ObservedTrajectory); i++ {
						ret = append(ret, c.ObservedTrajectory[i].StringSlice())
						ret = append(ret, c.ColocalizedTrajectory[i].StringSlice())
					}
				}
			}
		}

		return ret
	}(), ';'); err != nil {
		log.Fatal(err)
	}

	// --------------------------------------------------
	// Colocalized passive trajectories lengths
	// --------------------------------------------------

	if err := writeCSV(fmt.Sprintf("../data/output/%s_colocalized_passive_trajectories_lengths.csv", cfg.Filename), []string{"Trajectory length", "Count"}, func() [][]string {
		var ret [][]string

		keys := make([]int, 0, len(counters))
		for k := range counters {
			keys = append(keys, k)
		}
		sort.Ints(keys)

		for _, k := range keys {
			ret = append(ret, []string{strconv.Itoa(k), strconv.Itoa(counters[k])})
		}

		return ret
	}(), ';'); err != nil {
		log.Fatal(err)
	}

	// --------------------------------------------------
	// Active trajectories
	// --------------------------------------------------

	counters = make(map[int]int)

	if err := writeCSV(fmt.Sprintf("../data/output/%s_active_trajectories.csv", cfg.Filename), []string{"Id", "Frame", "X", "Y", "Intensity", "Color", "Movie"}, func() [][]string {
		var ret [][]string

		for _, tr := range t.Trajectories() {
			if !tr.IsRegionOfInterest(cfg.RegionOfInterestX, cfg.RegionOfInterestY, cfg.RegionOfInterestRadius) || tr.IsPassive(cfg.PassivityRadius) {
				continue
			}

			counters[tr.Len()] += 1

			for _, p := range tr.Points() {
				ret = append(ret, p.StringSlice())
			}
		}

		return ret
	}(), ';'); err != nil {
		log.Fatal(err)
	}

	// --------------------------------------------------
	// Active trajectories lengths
	// --------------------------------------------------

	if err := writeCSV(fmt.Sprintf("../data/output/%s_active_trajectories_lengths.csv", cfg.Filename), []string{"Trajectory length", "Count"}, func() [][]string {
		var ret [][]string

		keys := make([]int, 0, len(counters))
		for k := range counters {
			keys = append(keys, k)
		}
		sort.Ints(keys)

		for _, k := range keys {
			ret = append(ret, []string{strconv.Itoa(k), strconv.Itoa(counters[k])})
		}

		return ret
	}(), ';'); err != nil {
		log.Fatal(err)
	}

	// --------------------------------------------------
	// Passive trajectories
	// --------------------------------------------------

	counters = make(map[int]int)

	if err := writeCSV(fmt.Sprintf("../data/output/%s_passive_trajectories.csv", cfg.Filename), []string{"Id", "Frame", "X", "Y", "Intensity", "Color", "Movie"}, func() [][]string {
		var ret [][]string

		for _, tr := range t.Trajectories() {
			if !tr.IsRegionOfInterest(cfg.RegionOfInterestX, cfg.RegionOfInterestY, cfg.RegionOfInterestRadius) || !tr.IsPassive(cfg.PassivityRadius) {
				continue
			}

			counters[tr.Len()] += 1

			for _, p := range tr.Points() {
				ret = append(ret, p.StringSlice())
			}
		}

		return ret
	}(), ';'); err != nil {
		log.Fatal(err)
	}

	// --------------------------------------------------
	// Passive trajectories lengths
	// --------------------------------------------------

	if err := writeCSV(fmt.Sprintf("../data/output/%s_passive_trajectories_lengths.csv", cfg.Filename), []string{"Trajectory length", "Count"}, func() [][]string {
		var ret [][]string

		keys := make([]int, 0, len(counters))
		for k := range counters {
			keys = append(keys, k)
		}
		sort.Ints(keys)

		for _, k := range keys {
			ret = append(ret, []string{strconv.Itoa(k), strconv.Itoa(counters[k])})
		}

		return ret
	}(), ';'); err != nil {
		log.Fatal(err)
	}

	// --------------------------------------------------
	// Green trajectories if ROI specified
	// --------------------------------------------------

	counters = make(map[int]int)

	if cfg.RegionOfInterestX >= 0 || cfg.RegionOfInterestY >= 0 {
		if err := writeCSV(fmt.Sprintf("../data/output/%s_green_trajectories.csv", cfg.Filename), []string{"Id", "Frame", "X", "Y", "Intensity", "Color", "Movie"}, func() [][]string {
			var ret [][]string

			for _, tr := range t.Trajectories() {
				if !tr.IsRegionOfInterest(cfg.RegionOfInterestX, cfg.RegionOfInterestY, cfg.RegionOfInterestRadius) || tr.Color != color.Grn {
					continue
				}

				counters[tr.Len()] += 1

				for _, p := range tr.Points() {
					ret = append(ret, p.StringSlice())
				}
			}

			return ret
		}(), ';'); err != nil {
			log.Fatal(err)
		}
	}

	// --------------------------------------------------
	// Green trajectories lengths if ROI specified
	// --------------------------------------------------

	if err := writeCSV(fmt.Sprintf("../data/output/%s_green_trajectories_lengths.csv", cfg.Filename), []string{"Trajectory length", "Count"}, func() [][]string {
		var ret [][]string

		keys := make([]int, 0, len(counters))
		for k := range counters {
			keys = append(keys, k)
		}
		sort.Ints(keys)

		for _, k := range keys {
			ret = append(ret, []string{strconv.Itoa(k), strconv.Itoa(counters[k])})
		}

		return ret
	}(), ';'); err != nil {
		log.Fatal(err)
	}

	// --------------------------------------------------
	// Red trajectories if ROI specified
	// --------------------------------------------------

	counters = make(map[int]int)

	if cfg.RegionOfInterestX >= 0 || cfg.RegionOfInterestY >= 0 {
		if err := writeCSV(fmt.Sprintf("../data/output/%s_red_trajectories.csv", cfg.Filename), []string{"Id", "Frame", "X", "Y", "Intensity", "Color", "Movie"}, func() [][]string {
			var ret [][]string

			for _, tr := range t.Trajectories() {
				if !tr.IsRegionOfInterest(cfg.RegionOfInterestX, cfg.RegionOfInterestY, cfg.RegionOfInterestRadius) || tr.Color != color.Red {
					continue
				}

				counters[tr.Len()] += 1

				for _, p := range tr.Points() {
					ret = append(ret, p.StringSlice())
				}
			}

			return ret
		}(), ';'); err != nil {
			log.Fatal(err)
		}
	}

	// --------------------------------------------------
	// Red trajectories lengths if ROI specified
	// --------------------------------------------------

	if err := writeCSV(fmt.Sprintf("../data/output/%s_red_trajectories_lengths.csv", cfg.Filename), []string{"Trajectory length", "Count"}, func() [][]string {
		var ret [][]string

		keys := make([]int, 0, len(counters))
		for k := range counters {
			keys = append(keys, k)
		}
		sort.Ints(keys)

		for _, k := range keys {
			ret = append(ret, []string{strconv.Itoa(k), strconv.Itoa(counters[k])})
		}

		return ret
	}(), ';'); err != nil {
		log.Fatal(err)
	}

	fmt.Println("------------------ DONE ! -----------------")
}

func loadCSV(file string, xShift float64, yShift float64) (trajectory.Collection, error) {
	f, err := os.Open(file)
	if err != nil {
		return nil, err
	}
	defer func() {
		if err := f.Close(); err != nil {
			log.Fatal(err)
		}
	}()

	r := csv.NewReader(f)
	r.Comma = ';'

	t := trajectory.NewCollection()

	for {
		row, err := r.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			return nil, err
		}

		p := &point.Point{}
		if p.Id, err = strconv.ParseUint(row[0], 10, 64); err != nil {
			return nil, err
		}

		if p.Frame, err = strconv.ParseUint(row[1], 10, 64); err != nil {
			return nil, err
		}

		if p.XAxis, err = strconv.ParseFloat(strings.ReplaceAll(row[2], ",", "."), 64); err != nil {
			return nil, err
		}

		if p.YAxis, err = strconv.ParseFloat(strings.ReplaceAll(row[3], ",", "."), 64); err != nil {
			return nil, err
		}

		if p.Intensity, err = strconv.ParseFloat(strings.ReplaceAll(row[4], ",", "."), 64); err != nil {
			return nil, err
		}

		if row[5] == "" {
			return nil, fmt.Errorf("point id %d missing color attribute", p.Id)
		}

		c, err := color.Parse(row[5])
		if err != nil {
			return nil, fmt.Errorf("point id %d %s", p.Id, err)
		}
		p.Color = c

		if row[6] == "" {
			return nil, fmt.Errorf("point id %d missing movie attribute", p.Id)
		} else {
			p.Movie = row[6]
		}

		if p.Color == color.Red {
			p.XAxis += xShift
			p.YAxis += yShift
		}

		t.Add(p)
	}

	return t, nil
}

func writeCSV(filepath string, header []string, rows [][]string, comma rune) error {
	f, err := os.Create(filepath)
	if err != nil {
		return err
	}

	defer func() {
		if err := f.Close(); err != nil {
			log.Fatal(err)
		}
	}()

	w := csv.NewWriter(f)
	w.Comma = comma

	defer w.Flush()

	if err := w.Write(header); err != nil {
		return err
	}

	for _, r := range rows {
		if err := w.Write(r); err != nil {
			return err
		}
	}

	return nil
}

func printConfig(config *config.Config) {
	fmt.Println("---------- Configuration summary ----------")
	fmt.Println()
	fmt.Printf("%-20s: %s \n", "Filename", config.Filename)
	fmt.Printf("%-20s: %f \n", "Distance", config.Distance)
	fmt.Printf("%-20s: %d \n", "Blinking", config.Blinking)
	fmt.Printf("%-20s: %f \n", "ShiftX", config.ShiftX)
	fmt.Printf("%-20s: %f \n", "ShiftY", config.ShiftY)
	fmt.Printf("%-20s: %f \n", "PassivityRadius", config.PassivityRadius)
	fmt.Printf("%-20s: %f \n", "RegionOfInterestX", config.RegionOfInterestX)
	fmt.Printf("%-20s: %f \n", "RegionOfInterestY", config.RegionOfInterestY)
	fmt.Printf("%-20s: %f \n", "RegionOfInterestRadius", config.RegionOfInterestRadius)
	fmt.Println()
	fmt.Println("-------------------------------------------")
}
