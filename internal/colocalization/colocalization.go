package colocalization

import (
	"app/internal/color"
	"app/internal/point"
	"app/internal/trajectory"
	"math"
)

type Colocalization struct {
	ObservedTrajectory    []*point.Point
	ColocalizedTrajectory []*point.Point
}

func (c *Colocalization) Len() int {
	return len(c.ObservedTrajectory)
}

type collection struct {
	colocalizations []*Colocalization
}

func (c *collection) add(observedTrajectoryPoint *point.Point, colocalizedTrajectoryPoint *point.Point, blinking uint64) {
	for _, colocalization := range c.colocalizations {
		if math.Abs(float64(colocalization.ObservedTrajectory[colocalization.Len()-1].Frame)-float64(observedTrajectoryPoint.Frame)) <= float64(blinking) {
			colocalization.ObservedTrajectory = append(colocalization.ObservedTrajectory, observedTrajectoryPoint)
			colocalization.ColocalizedTrajectory = append(colocalization.ColocalizedTrajectory, colocalizedTrajectoryPoint)

			return
		}
	}

	c.colocalizations = append(c.colocalizations, &Colocalization{
		ObservedTrajectory:    []*point.Point{observedTrajectoryPoint},
		ColocalizedTrajectory: []*point.Point{colocalizedTrajectoryPoint},
	})
}

func Colocalizations(
	observedTrajectory *trajectory.Trajectory,
	colocalizedTrajectory *trajectory.Trajectory,
	distance float64,
	blinking uint64,
) []*Colocalization {
	if observedTrajectory == colocalizedTrajectory ||
		observedTrajectory.Color != color.Red ||
		observedTrajectory.Color == colocalizedTrajectory.Color ||
		observedTrajectory.Movie != colocalizedTrajectory.Movie {
		return nil
	}

	var collection = &collection{}

	for e := observedTrajectory.Timeline.Front(); e != nil; e = e.Next() {
		otp := e.Value.(*point.Point)

		for e := colocalizedTrajectory.Timeline.Front(); e != nil; e = e.Next() {
			ctp := e.Value.(*point.Point)

			if otp.Frame != ctp.Frame ||
				!otp.IsNeighbour(ctp, distance) {
				continue
			}

			collection.add(otp, ctp, blinking)
		}
	}

	var ret = make([]*Colocalization, 0)

	for _, c := range collection.colocalizations {
		if c.Len() > 0 {
			ret = append(ret, c)
		}
	}

	return ret
}
