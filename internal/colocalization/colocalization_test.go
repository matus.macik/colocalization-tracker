package colocalization

import (
	"app/internal/color"
	"app/internal/point"
	"app/internal/trajectory"
	"container/list"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestColocalizations(t *testing.T) {
	p1 := &point.Point{Id: 1, Frame: 1, XAxis: 1, YAxis: 1, Movie: "m-1", Color: color.Red}
	p2 := &point.Point{Id: 2, Frame: 1, XAxis: 1, YAxis: 1, Movie: "m-1", Color: color.Grn}
	p3 := &point.Point{Id: 1, Frame: 2, XAxis: 1, YAxis: 1, Movie: "m-1", Color: color.Red}
	p4 := &point.Point{Id: 2, Frame: 2, XAxis: 1, YAxis: 1, Movie: "m-1", Color: color.Grn}
	p5 := &point.Point{Id: 1, Frame: 4, XAxis: 1, YAxis: 1, Movie: "m-1", Color: color.Red}
	p6 := &point.Point{Id: 2, Frame: 4, XAxis: 1, YAxis: 1, Movie: "m-1", Color: color.Grn}
	p7 := &point.Point{Id: 1, Frame: 5, XAxis: 1, YAxis: 1, Movie: "m-1", Color: color.Red}
	p8 := &point.Point{Id: 2, Frame: 5, XAxis: 1, YAxis: 1, Movie: "m-1", Color: color.Grn}

	tr1 := &trajectory.Trajectory{Id: p1.Id, Movie: p1.Movie, Color: p1.Color, Timeline: list.New()}
	tr1.Timeline.PushBack(p1)
	tr1.Timeline.PushBack(p3)
	tr1.Timeline.PushBack(p5)
	tr1.Timeline.PushBack(p7)

	tr2 := &trajectory.Trajectory{Id: p2.Id, Movie: p2.Movie, Color: p2.Color, Timeline: list.New()}
	tr2.Timeline.PushBack(p2)
	tr2.Timeline.PushBack(p4)
	tr2.Timeline.PushBack(p6)
	tr2.Timeline.PushBack(p8)

	t.Run("blinking 1", func(t *testing.T) {
		c := Colocalizations(tr1, tr2, 1, 1)
		assert.Len(t, c, 2)
	})

	t.Run("blinking 2", func(t *testing.T) {
		c := Colocalizations(tr1, tr2, 1, 2)
		assert.Len(t, c, 1)
	})

	t.Run("wrong trajectories", func(t *testing.T) {
		c := Colocalizations(tr1, tr1, 1, 1)
		assert.Nil(t, c)
	})
}
