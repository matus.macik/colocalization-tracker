package color

import "errors"

var InvalidColorErr = errors.New("invalid color")

type Color string

const (
	Red Color = "RED"
	Grn Color = "GRN"
)

func Parse(value string) (Color, error) {
	switch value {
	case string(Red):
		return Red, nil
	case string(Grn):
		return Grn, nil
	}

	return "", InvalidColorErr
}
