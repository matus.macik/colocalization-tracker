package color

import (
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"testing"
)

func TestParse(t *testing.T) {
	t.Run("ok", func(t *testing.T) {
		tcs := []struct {
			input    string
			expColor Color
		}{
			{input: "RED", expColor: Red},
			{input: "GRN", expColor: Grn},
		}

		for _, tc := range tcs {
			color, err := Parse(tc.input)
			require.NoError(t, err)
			assert.Equal(t, tc.expColor, color)
		}
	})

	t.Run("invalid color", func(t *testing.T) {
		_, err := Parse("BLACK")
		assert.Error(t, err)
	})
}
