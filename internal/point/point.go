package point

import (
	"fmt"
	"math"
	"strconv"

	"app/internal/color"
)

type Point struct {
	Id        uint64
	Frame     uint64
	XAxis     float64
	YAxis     float64
	Intensity float64
	Color     color.Color
	Movie     string
}

func (p *Point) IsNeighbour(point *Point, distance float64) bool {
	if math.Sqrt(math.Pow(p.XAxis-point.XAxis, 2)+math.Pow(p.YAxis-point.YAxis, 2)) <= distance {
		return true
	}

	return false
}

func (p *Point) UniqueId() string {
	return fmt.Sprintf("%d_%s_%s", p.Id, p.Color, p.Movie)
}

func (p *Point) StringSlice() []string {
	return []string{
		strconv.Itoa(int(p.Id)),
		strconv.Itoa(int(p.Frame)),
		strconv.FormatFloat(p.XAxis, 'f', 5, 64),
		strconv.FormatFloat(p.YAxis, 'f', 5, 64),
		strconv.FormatFloat(p.Intensity, 'f', 5, 64),
		string(p.Color),
		p.Movie,
	}
}
