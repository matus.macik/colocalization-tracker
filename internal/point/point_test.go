package point

import (
	"app/internal/color"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestPoint_IsNeighbour(t *testing.T) {
	p1 := &Point{XAxis: 0, YAxis: 0}
	p2 := &Point{XAxis: 6, YAxis: 8}

	t.Run("is neighbour", func(t *testing.T) {
		assert.True(t, p1.IsNeighbour(p2, 10))
	})

	t.Run("is not neighbour", func(t *testing.T) {
		assert.False(t, p1.IsNeighbour(p2, 9))
	})
}

func TestPoint_UniqueId(t *testing.T) {
	p := &Point{Id: 1, Frame: 1, XAxis: 1, YAxis: 1, Movie: "m-1", Color: color.Red}
	assert.Equal(t, "1_RED_m-1", p.UniqueId())
}
