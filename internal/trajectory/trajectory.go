package trajectory

import (
	"app/internal/color"
	"app/internal/point"
	"container/list"
	"math"
	"sync"
)

type Trajectory struct {
	Id       uint64
	Movie    string
	Color    color.Color
	Timeline *list.List
}

func (t *Trajectory) Points() []*point.Point {
	var ps = make([]*point.Point, 0, t.Timeline.Len())

	for e := t.Timeline.Front(); e != nil; e = e.Next() {
		ps = append(ps, e.Value.(*point.Point))
	}

	return ps
}

func (t *Trajectory) Len() int {
	return t.Timeline.Len()
}

func (t *Trajectory) IsRegionOfInterest(x, y, radius float64) bool {
	if radius == 0 {
		return true
	}

	for e := t.Timeline.Front(); e != nil; e = e.Next() {
		p := e.Value.(*point.Point)

		if math.Sqrt(math.Pow(p.XAxis-x, 2)+math.Pow(p.YAxis-y, 2)) <= radius {
			return true
		}
	}

	return false
}

func (t *Trajectory) IsPassive(radius float64) bool {
	if radius == 0 {
		return false
	}

	var fp *point.Point

	for e := t.Timeline.Front(); e != nil; e = e.Next() {
		if fp == nil {
			fp = e.Value.(*point.Point)
			continue
		}

		p := e.Value.(*point.Point)

		if math.Sqrt(math.Pow(fp.XAxis-p.XAxis, 2)+math.Pow(fp.YAxis-p.YAxis, 2)) >= radius {
			return false
		}
	}

	return true
}

type Collection interface {
	Add(p *point.Point)
	Trajectories() []*Trajectory
}

type collection struct {
	ts    map[string]*Trajectory
	mutex sync.Mutex
}

func NewCollection() Collection {
	return &collection{
		ts: make(map[string]*Trajectory, 0),
	}
}

func (c *collection) Add(p *point.Point) {
	c.mutex.Lock()
	defer c.mutex.Unlock()

	t, ok := c.ts[p.UniqueId()]
	if !ok {
		t = &Trajectory{
			Id:       p.Id,
			Movie:    p.Movie,
			Color:    p.Color,
			Timeline: list.New(),
		}
	}

	for e := t.Timeline.Front(); e != nil; e = e.Next() {
		if e.Value.(*point.Point).Frame > p.Frame {
			t.Timeline.InsertBefore(p, e)
			c.ts[p.UniqueId()] = t
			return
		}
	}

	t.Timeline.PushBack(p)
	c.ts[p.UniqueId()] = t
}

func (c *collection) Trajectories() []*Trajectory {
	c.mutex.Lock()
	defer c.mutex.Unlock()

	var ts = make([]*Trajectory, 0, len(c.ts))

	for _, t := range c.ts {
		ts = append(ts, t)
	}

	return ts
}
