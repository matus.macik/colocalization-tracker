package trajectory

import (
	"app/internal/color"
	"app/internal/point"
	"container/list"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestTrajectory_Points(t *testing.T) {
	p1 := &point.Point{Id: 1, Frame: 1, XAxis: 1, YAxis: 1, Movie: "m-1", Color: color.Red}
	p2 := &point.Point{Id: 1, Frame: 2, XAxis: 1, YAxis: 1, Movie: "m-1", Color: color.Red}
	p3 := &point.Point{Id: 1, Frame: 3, XAxis: 1, YAxis: 1, Movie: "m-1", Color: color.Red}
	p4 := &point.Point{Id: 1, Frame: 4, XAxis: 1, YAxis: 1, Movie: "m-1", Color: color.Red}

	tr := Trajectory{Id: 1, Movie: "m-1", Color: color.Red, Timeline: list.New()}
	tr.Timeline.PushBack(p1)
	tr.Timeline.PushBack(p2)
	tr.Timeline.PushBack(p3)
	tr.Timeline.PushBack(p4)

	expPoints := []*point.Point{p1, p2, p3, p4}

	assert.Equal(t, expPoints, tr.Points())
}

func TestTrajectory_Len(t *testing.T) {
	p1 := &point.Point{Id: 1, Frame: 1, XAxis: 1, YAxis: 1, Movie: "m-1", Color: color.Red}
	p2 := &point.Point{Id: 1, Frame: 2, XAxis: 1, YAxis: 1, Movie: "m-1", Color: color.Red}

	tr := Trajectory{Id: 1, Movie: "m-1", Color: color.Red, Timeline: list.New()}
	tr.Timeline.PushBack(p1)
	tr.Timeline.PushBack(p2)

	assert.Equal(t, 2, tr.Len())
}

func TestTrajectory_IsRegionOfInterest(t *testing.T) {
	t.Run("is", func(t *testing.T) {
		tr := Trajectory{Id: 1, Movie: "m-1", Color: color.Red, Timeline: list.New()}
		tr.Timeline.PushBack(&point.Point{Id: 1, Frame: 1, XAxis: 3, YAxis: 4, Movie: "m-1", Color: color.Red})
		tr.Timeline.PushBack(&point.Point{Id: 1, Frame: 2, XAxis: 2, YAxis: 3, Movie: "m-1", Color: color.Red})
		tr.Timeline.PushBack(&point.Point{Id: 1, Frame: 3, XAxis: 1, YAxis: 1, Movie: "m-1", Color: color.Red})

		assert.True(t, tr.IsRegionOfInterest(1.3, 0.9, 0.5))
	})

	t.Run("is not", func(t *testing.T) {
		tr := Trajectory{Id: 1, Movie: "m-1", Color: color.Red, Timeline: list.New()}
		tr.Timeline.PushBack(&point.Point{Id: 1, Frame: 1, XAxis: 3, YAxis: 4, Movie: "m-1", Color: color.Red})
		tr.Timeline.PushBack(&point.Point{Id: 1, Frame: 1, XAxis: 2, YAxis: 3, Movie: "m-1", Color: color.Red})
		tr.Timeline.PushBack(&point.Point{Id: 1, Frame: 1, XAxis: 2, YAxis: 1, Movie: "m-1", Color: color.Red})

		assert.False(t, tr.IsRegionOfInterest(1, 1, 0.5))
	})

	t.Run("zero radius", func(t *testing.T) {
		tr := Trajectory{}

		assert.True(t, tr.IsRegionOfInterest(0, 0, 0))
	})
}

func TestTrajectory_IsPassive(t *testing.T) {
	t.Run("is", func(t *testing.T) {
		tr := Trajectory{Id: 1, Movie: "m-1", Color: color.Red, Timeline: list.New()}
		tr.Timeline.PushBack(&point.Point{Id: 1, Frame: 1, XAxis: 1, YAxis: 1, Movie: "m-1", Color: color.Red})
		tr.Timeline.PushBack(&point.Point{Id: 1, Frame: 2, XAxis: 1, YAxis: 1, Movie: "m-1", Color: color.Red})
		tr.Timeline.PushBack(&point.Point{Id: 1, Frame: 3, XAxis: 1, YAxis: 1, Movie: "m-1", Color: color.Red})
		tr.Timeline.PushBack(&point.Point{Id: 1, Frame: 4, XAxis: 1, YAxis: 1, Movie: "m-1", Color: color.Red})

		assert.True(t, tr.IsPassive(0.5))
	})

	t.Run("is not", func(t *testing.T) {
		tr := Trajectory{Id: 1, Movie: "m-1", Color: color.Red, Timeline: list.New()}
		tr.Timeline.PushBack(&point.Point{Id: 1, Frame: 1, XAxis: 1.1, YAxis: 1, Movie: "m-1", Color: color.Red})
		tr.Timeline.PushBack(&point.Point{Id: 1, Frame: 2, XAxis: 1.2, YAxis: 1, Movie: "m-1", Color: color.Red})
		tr.Timeline.PushBack(&point.Point{Id: 1, Frame: 3, XAxis: 1.4, YAxis: 1, Movie: "m-1", Color: color.Red})
		tr.Timeline.PushBack(&point.Point{Id: 1, Frame: 4, XAxis: 1.8, YAxis: 1, Movie: "m-1", Color: color.Red})

		assert.False(t, tr.IsPassive(0.5))
	})

	t.Run("zero radius", func(t *testing.T) {
		tr := Trajectory{}

		assert.False(t, tr.IsPassive(0))
	})

	t.Run("empty timeline", func(t *testing.T) {
		tr := Trajectory{Id: 1, Movie: "m-1", Color: color.Red, Timeline: list.New()}

		assert.True(t, tr.IsPassive(0.5))
	})
}

func TestNewTrajectoryCollection(t *testing.T) {
	c := NewCollection()

	assert.Equal(t, &collection{
		ts: make(map[string]*Trajectory, 0),
	}, c)
}

func TestTrajectoryCollection_Add(t *testing.T) {
	p1 := &point.Point{Id: 1, Frame: 1, XAxis: 1, YAxis: 1, Movie: "m-1", Color: color.Red}
	p2 := &point.Point{Id: 1, Frame: 3, XAxis: 3, YAxis: 3, Movie: "m-1", Color: color.Red}
	p3 := &point.Point{Id: 1, Frame: 2, XAxis: 2, YAxis: 2, Movie: "m-1", Color: color.Red}
	p4 := &point.Point{Id: 2, Frame: 2, XAxis: 2, YAxis: 2, Movie: "m-1", Color: color.Grn}

	t1 := &Trajectory{Id: p1.Id, Movie: p1.Movie, Color: p1.Color, Timeline: list.New()}
	t1.Timeline.PushBack(p1)
	t1.Timeline.PushBack(p3)
	t1.Timeline.PushBack(p2)

	t2 := &Trajectory{Id: p4.Id, Movie: p4.Movie, Color: p4.Color, Timeline: list.New()}
	t2.Timeline.PushBack(p4)

	expCollection := &collection{
		ts: map[string]*Trajectory{
			"1_RED_m-1": t1,
			"2_GRN_m-1": t2,
		},
	}

	c := &collection{ts: make(map[string]*Trajectory, 0)}
	c.Add(p1)
	c.Add(p2)
	c.Add(p3)
	c.Add(p4)

	assert.Equal(t, expCollection, c)
}

func TestTrajectoryCollection_Trajectories(t *testing.T) {
	p1 := &point.Point{Id: 1, Frame: 1, XAxis: 1, YAxis: 1, Movie: "m-1", Color: color.Red}
	p2 := &point.Point{Id: 2, Frame: 1, XAxis: 1, YAxis: 1, Movie: "m-1", Color: color.Grn}

	t1 := &Trajectory{Id: p1.Id, Movie: p1.Movie, Color: p1.Color, Timeline: list.New()}
	t1.Timeline.PushBack(p1)

	t2 := &Trajectory{Id: p2.Id, Movie: p2.Movie, Color: p2.Color, Timeline: list.New()}
	t2.Timeline.PushBack(p2)

	expTrajectories := []*Trajectory{t1, t2}

	c := &collection{ts: make(map[string]*Trajectory, 0)}
	c.Add(p1)
	c.Add(p2)

	assert.Equal(t, expTrajectories, c.Trajectories())
}
